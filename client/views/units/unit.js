Template.Unit.onCreated(function(){
	this.unitEditMode = new ReactiveVar(false);
});

Template.Unit.helpers({
	editMode: function(){
		return Template.instance().unitEditMode.get();
	}
});

Template.Unit.events({
	
    'submit form' (event, template) {
        event.preventDefault();
		var target = event.target;
        // Get the Unit's Data from the form
        var unitNumber = target.unitNumber.value;
        var sqFootage = target.sqFootage.value;
        var residents = target.residents.value;
        var tenant = target.tenant.value;
        // Validation
        if(unitNumber.trim().length > 0 && sqFootage.trim().length >0) {
            var unitData = {
                unitNumber: unitNumber,
                squareFootage: sqFootage,
                residents: residents || 0,
                updatedAt: new Date()
            }
            if(tenant.trim().length > 0) {
                if(GlobalHelpers.validateEmail(tenant)) {
                    unitData.tenant = tenant;
                } else {
                    alert("Invalid Email");
                    return;
                }
            } else {
                unitData.tenant = null;
            }
            console.log("id: ", template.data._id);
            // Attempt to Insert.
            Meteor.call('updateUnit', FlowRouter.getParam('id'), template.data._id, unitData, function(err, result){
                if(err) {
                    console.log("Error Occured: ", err);
                    alert("Some Fields are Missing Or Have Unmatched Types. Please Recheck and then Submit");
                    return;
                }
                if(unitData.tenant) {
                    const user = Meteor.users.findOne({
                                    emails: { 
                                        $elemMatch: { address: unitData.tenant} 
                                    }
                                });
                    console.log("user: ", user);
                    if(user) {
                        Meteor.subscribe('UpdateTenantUser', user, {
                            "tenant": true,
                            "updatedAt": new Date()
                        }, {
                            onReady: function () {
                                alert("User is already Registered and has Become a Tenant");
                                alert("Unit Updated Successfully");
                            },
                            onError: function () {
                                alert("Something went Wrong while making User a Tenant"); 
                            }
                        });
                        
                    } else {
                        // Generate Password for Tenant New Account 
                        var password = Random.id(8);
                        // Create Tenant Account
                        Meteor.subscribe('InsertTenantUser', unitData.tenant, password, {
                            onReady: function () {
                                // Send Password to Tenant via Email
                                Meteor.call('sendEmailToTenant', unitData.tenant, password, function(err){
                                    if(err) {
                                        console.log("Error Occured While Sending an Email to Tenant Address: ", err);
                                        alert("Error Occured While Sending an Email to Tenant Address");
                                    }
                                    console.log("An email has been sent to the Tenant Address");
                                    alert("Tenant Account has been Created And Password has been sent to the Tenant Email Address");
                                    alert("Unit Updated Successfully");
                                });
                            },
                            onError: function () {
                                alert("Something went Wrong while Creating a Tenant User"); 
                            }
                        });
                    }
                } else {
                    alert("Unit Updated Successfully");
                }
            });
            //Close the Edit Mode and Show the trash Icon
            template.unitEditMode.set(!template.unitEditMode.get());
            template.$(".fa-trash").show();

        } else {
            alert("Unit Number and Square Footage are Required Fields. Please Fill them");
        }
    },
    'click .fa-trash': function (event, template) {
		Meteor.call('deleteUnit', FlowRouter.getParam('id'), template.data._id, function(err, result){
            if(err) {
                console.log("Error Occured: ", err);
                alert("Something went wrong, While deleting the Unit. Please Try Again");
                return;
            }
            alert("Deleted Successfully");
        });
	},
	'click .fa-pencil': function (event, template) {
        console.log("template: ", template);
		template.unitEditMode.set(!template.unitEditMode.get());
        if(!template.unitEditMode.get()) {
            template.$(".fa-trash").show();
        } else {
            template.$("#bLink").removeAttr("href");
            template.$(".fa-trash").hide();
        }
	}
});