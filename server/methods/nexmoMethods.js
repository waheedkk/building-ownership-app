// Import Nexmo Module
import Nexmo from 'nexmo';
var Future = Npm.require("fibers/future");

// Initialization of Nexmo Services 
const nexmo = new Nexmo({
    apiKey: Meteor.settings.private.nexmo.apiKey,
    apiSecret: Meteor.settings.private.nexmo.apiSecret
}, {
    debug: true
});

Meteor.methods({

    'getVerificationRequestId': function(phoneNumber) {
        
        var nexmoVerify_Sync = Meteor.wrapAsync( function (phoneNumber, callback) {
            nexmo.verify.request ({
                    number: phoneNumber,
                    brand:'sampleApp'
            }, function (err, result) {
                if(err) {
                    console.log("error: ", err);
                    callback(err, null);
                }
                console.log("result: ", result.request_id);
                callback(null, result.request_id);
            });
        });

        var request_id =  nexmoVerify_Sync(phoneNumber);
        if(request_id) {
            Meteor.users.update({_id: this.userId}, {
                $set: {
                    "verificationRequestId": request_id
                }
            });
            return;
        } else {
            throw new Meteor.Error("Nexmo Verify Request API Error");
        }
    },

    'getAccountVerified': function (verificationRequestId, verificationCode) {

        var nexmoVerifyCheck_Sync = Meteor.wrapAsync( function (verificationRequestId, verificationCode, callback) {
            nexmo.verify.check({
                request_id: verificationRequestId,
                code: verificationCode
            }, function(err, result){
                if(err) {
                console.log(err);
                callback(err, null);
                } else {
                console.log(result);
                callback(null, result);
                }
            });
        });

        var response =  nexmoVerifyCheck_Sync(verificationRequestId, verificationCode);
        if(response.status && response.status == 0) {
            return;
        } else {
            throw new Meteor.Error("Nexmo Verify Check API Error");
        }
    }

});