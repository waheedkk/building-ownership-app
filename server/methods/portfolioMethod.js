// This File Contains CRUD Methods for Portfolio Module

Meteor.methods({

    // Insert Portfolio Method
    'insertPortfolio': function(portfolioName) {
        Portfolios.insert({
			name: portfolioName
		});
    },
    
    // Update Portfolio Method
    'updatePortfolio': function (portfolioId, portfolioName) {
        try{
            Portfolios.update(portfolioId, {
                $set: {
                    name: portfolioName
                }
            });
        } catch (exception) {
            console.log(exception);
        }
    },

    // Delete Portfolio Method
    'deletePortfolio': function (id) {
        try{
            Buildings.remove( { portfolioId: id }, function(err, result) {
                if(err) {
                    console.log(err);
                } else {
                    Portfolios.remove(id);
                }
            });
        } catch (exception) {
            console.log(exception);
        }
    }

});