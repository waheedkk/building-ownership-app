Template.Portfolio.onCreated(function(){
	this.portfolioEditMode = new ReactiveVar(false);
});

Template.Portfolio.helpers({
	updatePortfolioId: function() {
		return this._id;
	},
	editMode: function(){
		return Template.instance().portfolioEditMode.get();
	}
});

Template.Portfolio.events({
	
    'submit form' (event, template) {
        event.preventDefault();
		var target = event.target;
        console.log("target: ", target);
        // Get the Building's Name and Address from the form
        var portfolioName = target.portfolioUpdatedName.value;
        // Attempt to update.
        Meteor.call('updatePortfolio', this._id, portfolioName, function(err, result){
            if(err) {
                console.log("Error Occured: ", err);
                alert("Some Fields are Missing Or Have Unmatched Types. Please Recheck and then Submit");
                return;
            }
            alert("Updated Successfully");
        });
        //Close the Edit Mode and Show the trash Icon
        template.portfolioEditMode.set(!template.portfolioEditMode.get());
        showHideIcons(template, this._id);
    },
    'click .fa-trash': function () {
		Meteor.call('deletePortfolio', this._id, function(err, result){
            if(err) {
                console.log("Error Occured: ", err);
                alert("Something went wrong, While deleting the portfolio. Please Try Again");
                return;
            }
            alert("Deleted Successfully");
        });
	},
	'click .fa-pencil': function (event, template) {
		template.portfolioEditMode.set(!template.portfolioEditMode.get());
        if(!template.portfolioEditMode.get()) {
            showHideIcons(template, this._id);
        } else {
            template.$("#"+ this._id).removeAttr("href");
            template.$(".fa-trash").hide();
        }
	}
});

// Make Code reusable :-)
var showHideIcons =  function(template, id){
    template.$("#"+ id).attr('href', '/portfolio/' + id + '/buildings');
    template.$(".fa-trash").show();
}