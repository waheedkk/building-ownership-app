Meteor.methods({

  'activateAccount': function(credentials) {
      
      console.log("credentials: ", credentials);
      var user = Accounts.findUserByEmail(credentials.email);
      console.log(user);
      if(user.verificationRequestId && credentials.verificationCode) {
        Meteor.call('getAccountVerified', user.verificationRequestId, credentials.verificationCode, function(err, result){
          if(err) {
              console.log("err: ", err);
              throw new Meteor.Error("Something Bad happened while verifying the Code");
          }
          Meteor.users.update(user, {
            $set: {
              "verified": true,
              "emails.0.verified" :true,
              "UpdatedAt": new Date()
            }
          });
          return;
        });
      } else {
        throw new Meteor.Error("verification Code is Invalid");
      }
  }
});
