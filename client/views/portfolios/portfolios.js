Template.Portfolios.onCreated(function(){
	var self = this;
	self.autorun(function() {
		// Subscribe to Portfolios and User
        // Template Level Subscription
		self.subscribe('portfolios');
		var subs = self.subscribe('FindUserByEmailId');

		if(!!Meteor.userId()) {
			if (subs.ready()) {
				if(Meteor.user().verified) {
					if(Meteor.user().owner) {
						FlowRouter.go('/portfolios');
					} else if(Meteor.user().tenant) {
						FlowRouter.go('/profilesettings');
					}
				} else {
					FlowRouter.go('/updatephone');
				}
			}
		} else {
			FlowRouter.go('/');
		}
	});
});

Template.Portfolios.helpers({
	
	portfolios: ()=> {
		return Portfolios.find({});
	}
});

Template.Portfolios.events({
	'click .new-element': () => {
		Session.set('newPortfolio', true);
	}
});