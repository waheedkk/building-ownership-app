// This File Contains Methods for Tenant Module
//import transporter from '../main.js'

Meteor.methods({

    'sendEmailToTenant': function (receiverEmail, password) {

        // Send Email to Tenant Account for Password Info
        try {
            Email.send({
                to: receiverEmail, // receiver Email
                from: Meteor.settings.private.gmail.email, // sender address,
                subject: 'Your Tenant Account Password', // Subject line
                text: "Your Password for Tenant Account is :" + password, // @email Body
            });

            console.log("Email Sent");
        } catch(exception){
            console.log(exception);
        }
    }

});