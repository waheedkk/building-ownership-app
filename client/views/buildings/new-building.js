Template.NewBuilding.events({
	'click .fa-close': function() {
		Session.set('newBuilding', false);
	},
	
    'submit form' (event, template) {
        event.preventDefault();
		var target = event.target;
        // Get the Building's Name and Address from the form
		console.log("target: ", target);
        var buildingName = target.name.value;
        var buildingAddress = target.address.value;
        // Attempt to Insert.
		Meteor.call('insertBuilding', buildingName, buildingAddress, FlowRouter.getParam('portfolioId'), function(err, result){
			if(err) {
				console.log("Error Occured: ", err);
				alert("Some Fields are Missing Or Have Unmatched Types. Please Recheck and then Submit");
				return;
			}
			// Clear Input Fields Finally
			template.find("form").reset();
			Session.set('newBuilding', false);
		});
    }
});