Template.LogIn.onCreated(function(){
	var self = this;
	self.autorun(function() {
        // Template Level Subscription
		self.subscribe('FindUserByEmailId');
	});
});

Template.LogIn.events({
    
    'submit form' (event, template) {
        event.preventDefault();
        // Get the username and password from the form
        var email = event.target.email.value;
        var password = event.target.password.value;
        // Attempt to login.
        Meteor.loginWithPassword(email, password, function(error) {
            // Handle the response
            if( Meteor.user() ) {
                if ( Meteor.user().emails[0].verified ) {
                    // Redirect the user to where they're loggin into
                    if(Meteor.user().owner) {
                        FlowRouter.go("/portfolios");
                    } else if(Meteor.user().tenant) {
                        FlowRouter.go("/profilesettings");
                    }
                    
                } else if (((Meteor.user().tenant && !Meteor.user().verified) || (Meteor.user().owner && !Meteor.user().verified)) ) {
                    FlowRouter.go("/updatephone");
                }
            } else {
                // If no user resulted from the attempt, an error variable will be available
                // in this callback. We can output the error to the user here.
                var message = "There was an error logging in: <strong>" + error.reason + "</strong>";

                template.find('#form-messages').html(message);
            }
            return;
        });
    },

    'click #activate': function(){
        console.log("Hello");
        FlowRouter.go("/activate");
    }
});