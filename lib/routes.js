// handling / route
FlowRouter.route('/', {
    name: 'home',
    action() {
        BlazeLayout.render('Home');
    },
    triggersEnter: [function(context, redirect) {
        if(!!Meteor.userId()) {
            redirect('/portfolios');
        } else {
            route = FlowRouter.current();
        }
    }]
});

// handling /activate route
FlowRouter.route('/activate', {
    name: 'activate',
    action() {
        BlazeLayout.render('Activate');
    }
});

// handling /Updatephone route
FlowRouter.route('/updatephone', {
    name: 'updatephone',
    action() {
        BlazeLayout.render('UpdatePhone');
    },
    triggersEnter: [routeInitTrigger]
});

// handling /portfolios route
FlowRouter.route('/portfolios', {
    action() {
        BlazeLayout.render('MainLayout', {
            main: 'Portfolios'
        });
    },
    triggersEnter: [routeInitTrigger]
});

// handling /profilesettings
FlowRouter.route('/profilesettings', {
    name: 'profilesettings',
    action() {
        BlazeLayout.render('MainLayout', {
            main: 'ProfileSettings'
        });
    },
    triggersEnter: [routeInitTrigger]
});

var portfolioRoutes = FlowRouter.group({
    prefix: '/portfolio',
    name: 'portfolio',
    triggersEnter: [routeInitTrigger]
});

// handling /portfolio/:portfolioId/buildings route
portfolioRoutes.route('/:portfolioId/buildings', {
    action() {
        BlazeLayout.render('MainLayout', {
            main: 'Buildings'
        });
    }
});

// handling /portfolio/:portfolioId/building/:id route
portfolioRoutes.route('/:portfolioId/building/:id', {
    action() {
        BlazeLayout.render('MainLayout', {
            main: 'Units'
        });
    }
});

// handling Not Found Pages
FlowRouter.notFound = {
    name: 'NotFound',
    action() {
        BlazeLayout.render('NotFound');
    }
};

// Common Trigger Method for Secure Routes
function routeInitTrigger(context, redirect) {
    if(!!Meteor.userId()) {
        route = FlowRouter.current()
    } else {
        redirect('/');
    }
}
