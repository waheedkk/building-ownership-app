// This File Contains CRUD Methods for Building Module

Meteor.methods({

    // Insert Building Method
    'insertBuilding': function(buildingName, buildingAddress, portfolioId) {
        Buildings.insert({
			name: buildingName,
			address: buildingAddress,
            portfolioId: portfolioId
		});
    },
    
    // Update Building Method
    'updateBuilding': function (buildingId, portfolioId, buildingName, buildingAddress) {
        Buildings.update({
            '_id': buildingId,
            'portfolioId': portfolioId
        }, {
            $set: {
                name: buildingName, 
                address: buildingAddress
            },
        });
    },

    // Delete Building Method
    'deleteBuilding': function (id) {
        Buildings.remove(id);
    }

});
