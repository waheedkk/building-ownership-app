Template.SideNav.onCreated(function(){
	var self = this;
	self.autorun(function() {
        // Template Level Subscription
		self.subscribe('FindUserByEmailId');
	});
});

Template.SideNav.helpers({
	
	isOwner: ()=> {
        console.log("Meteor.user(): ", Meteor.user());
		if(Meteor.user() && Meteor.user().owner) {
            return true;
        }
	},
	portfolioId: ()=> {
		if(FlowRouter.getParam('portfolioId')) {
            return FlowRouter.getParam('portfolioId');
        } else {
			return false;
		}
	}
});