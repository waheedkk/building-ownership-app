// Publish Buildings of current Loghed-In User
Meteor.publish('buildings', function(){
    return Buildings.find({ownerId: this.userId});
});

// Publish a Single Buildings
Meteor.publish('singleBuilding', function(buildingId){
    return Buildings.find({_id: buildingId});
}); 