// This File Contains CRUD Methods and Common Methods for Units Module
import _ from 'lodash';

Meteor.methods({

    //Method to Insert Unit Into Building
    'insertUnit': function(buildingId, unitData) {

        try{
            Buildings.update(buildingId, {
                $push: {
                    units: unitData
                }
            });
        } catch(exception){
            console.log(exception);
        }
    },

    // Update Unit Method
    'updateUnit': function (buildingId, unitId, unitUpdatedData) {
        try{
            var building = Buildings.findOne({_id: buildingId});
            var units = building.units;
            var unitToUpdate = _.find(units, ['_id', unitId]);
            var index = _.findIndex(units, ['_id', unitId]);
            unitUpdatedData = _.merge(unitToUpdate, unitUpdatedData);
            // Replace item at index using native splice and Update Building's Units
            units.splice(index, 1, unitUpdatedData);
            Buildings.update(buildingId, {
                $set: {
                    units: units
                }
            });
        } catch (exception) {
            console.log(exception);
        }
    },

    // Delete Unit Method
    'deleteUnit': function (buildingId, unitId) {
        try{
            Buildings.update(buildingId, {
                $pull: {
                    units: {_id: unitId}
                }
            });
        } catch (exception) {
            console.log(exception);
        }
        
    }
});