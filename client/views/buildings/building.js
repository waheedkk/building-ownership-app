Template.Building.onCreated(function(){
	this.editMode = new ReactiveVar(false);
});

Template.Building.helpers({
	updateBuildingId: function() {
		return this._id;
	},
	editMode: function(){
		return Template.instance().editMode.get();
	},
    portfolioId: function(){
        return FlowRouter.getParam('portfolioId');
    } 

});

Template.Building.events({
	
    'submit form' (event, template) {
        event.preventDefault();
		var target = event.target;
        console.log("target: ", target);
        // Get the Building's Name and Address from the form
        var buildingName = target.updatedName.value;
        var buildingAddress = target.updatedAddress.value;
        // Attempt to update.
        Meteor.call('updateBuilding', this._id, FlowRouter.getParam('portfolioId'), buildingName, buildingAddress, function(err, result){
            if(err) {
                console.log("Error Occured: ", err);
                alert("Some Fields are Missing Or Have Unmatched Types. Please Recheck and then Submit");
                return;
            }
            alert("Updated Successfully");
        });
        //Close the Edit Mode and Show the trash Icon
        template.editMode.set(!template.editMode.get());
        showHideIcons(template, this._id);
    },
    'click .fa-trash': function () {
		Meteor.call('deleteBuilding', this._id, function(err, result){
            if(err) {
                console.log("Error Occured: ", err);
                alert("Something went wrong, While deleting the Building. Please Try Again");
                return;
            }
            alert("Deleted Successfully");
        });
	},
	'click .fa-pencil': function (event, template) {
		template.editMode.set(!template.editMode.get());
        if(!template.editMode.get()) {
            showHideIcons(template, this._id);
        } else {
            template.$("#"+ this._id).removeAttr("href");
            template.$(".fa-trash").hide();
        }
	}
});

// Make Code reusable :-)
var showHideIcons =  function(template, id){
    template.$("#"+ id).attr('href', '/portfolio/building/' + id);
    template.$(".fa-trash").show();
}