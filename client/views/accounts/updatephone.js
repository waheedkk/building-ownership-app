Template.UpdatePhone.events({

    'submit form' (event, template) {
        event.preventDefault();
		var target = event.target;
        // Get the Unit's Data' from the form
        var phoneNumber = target.phoneNumber.value;
        // Validation
        if(phoneNumber.trim().length > 0) {
            // Call Nexmo Method and get a Verification Code
            Meteor.call('getVerificationRequestId', phoneNumber, function(err, VerificationRequestId){
                if(err) {
                    console.log("err: ", err);
                    alert("Something Went Wrong while Processing Phone Number");
                    return;
                } else {
                    console.log("VerificationRequestId: ", VerificationRequestId);

                    Meteor.subscribe('UpdateCurrentUser', {
                        "phoneNumber": phoneNumber,
                        "updatedAt": new Date()
                    }, {
                        onReady: function () {
                            alert("An activation Code has been sent to your Number. Please Check");
                            FlowRouter.go("/activate");
                        },
                        onError: function () {
                            alert("Something went Wrong while making User a Tenant"); 
                        }
                    });
                }
            });
            // Clear Input Fields Finally
            template.find("form").reset();
        } else {
            alert("Phone Number is Required");
        }
    }
});