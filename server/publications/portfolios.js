// Publish Portfolios of current Logged-In User
Meteor.publish('portfolios', function(){
    return Portfolios.find({ownerId: this.userId});
});