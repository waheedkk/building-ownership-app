import { Accounts } from 'meteor/accounts-base'

Template.SignUp.events({
    // SignUp Event on Button Click
    'submit .form-bottom'(event, instance) {
        event.preventDefault();
        var firstName = event.target.firstName.value;
        var email = event.target.email.value;
        var phoneNumber = event.target.phoneNumber.value;
        var password = event.target.password.value;
        // Create and Store the User
        Accounts.createUser({
            firstName: firstName,
            phoneNumber: phoneNumber,
            email: email,
            password: password
        }, function(err){
            if(err) {
                console.log("Something went wrong: ", err);
            } else {
                Meteor.call('getVerificationRequestId', phoneNumber, function(err, VerificationRequestId){
                    if(err) {
                        console.log("err: ", err);
                        alert("Something Went Wrong while Processing Phone Number");
                        return;
                    }
                    alert("An activation Code has been sent to your Number. Please Check");
                    FlowRouter.go("/activate");
                });
            }
        });
    }
});