Template.Activate.onCreated(function(){
	var self = this;
	self.autorun(function() {
        // Template Level Subscription
		self.subscribe('FindUserByEmailId');
	});
});

Template.Activate.events({
    
    'submit form'(event, instance) {
        event.preventDefault();
        var self = this;
        var email = event.target.email.value;
        var verificationCode = event.target.code.value;

        Meteor.call('activateAccount', {
            email: email,
            verificationCode: verificationCode
        }, function(err, result){
            if(err) {
                console.log("Error Occured: ", err);
                alert("Verification Failed. Please Try Again");
                return;
            }
            alert("Congratulations!! Your Account has been Verified");
            if(Meteor.user().owner) {
                FlowRouter.go("/portfolios");
            } else if(Meteor.user().tenant) {
                FlowRouter.go("/profilesettings");
            }
        });
    }
});