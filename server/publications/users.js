// This File contains Publications related to Users Collection

// Publish Method for 'FindUserByEmailId'
Meteor.publish('FindUserByEmailId', function(email){
    try {
         return Meteor.users.find({}, {emails: 1});
    } catch (exception) {
        console.log(exception);
    }
});

// Publish Meteor for 'Inserting/Creating Tenant User'
Meteor.publish('InsertTenantUser', function(email, password){
    try {
    //     Meteor.users.insert({
    //         email: email,
    //         password: password,
    //         verified: false,
    //         owner: false,
    //         tenant: true,
    //         createdAt: new Date()
    //   });
        Accounts.createUser({
            email: email,
            password: password
        });
        this.ready();
    } catch (exception) {
        console.log(exception);
    }
});

// Publish Meteor for 'Updating Current Logged In User'
Meteor.publish('UpdateCurrentUser', function(updatedData){
    try {
        Meteor.users.update({_id: this.userId}, {
            $set: updatedData
        });
        this.ready();
    } catch (exception) {
        console.log(exception);
    }
});

// Publish Meteor for 'Updating Tenant User'
Meteor.publish('UpdateTenantUser', function(user, updatedData){
    try {
        Meteor.users.update(user, {
        $set: updatedData
      });
      this.ready();
    } catch (exception) {
        console.log(exception);
    }
});