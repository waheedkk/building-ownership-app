Template.Home.onCreated(function(){
	var self = this;
	self.autorun(function() {
		// Template Level Subscription
		var subs = self.subscribe('FindUserByEmailId');
        if(!!Meteor.userId()) {
            if (subs.ready()) {
                if(Meteor.user().verified) {
                    if(Meteor.user().owner) {
                        FlowRouter.go('/portfolios');
                    } else if(Meteor.user().tenant) {
                        FlowRouter.go('/profilesettings');
                    }
                } else {
                    FlowRouter.go('/updatephone');
                }
            }
        } else {
            route = FlowRouter.current();
        }
	});
});