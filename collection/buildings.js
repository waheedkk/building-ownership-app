Buildings = new Mongo.Collection('buildings');

Buildings.allow({
    insert: function(userId, doc) {
        return !!userId;
    },
	update: function(userId, doc) {
		return !!userId;
	}
});


BuildingSchema = new SimpleSchema({
    
    name: {
        type: String, optional: false
    },
    address: {
        type: String, optional: false
    },
    portfolioId: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: false
    },
    ownerId: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: false,
        autoValue: function () {
			return this.userId
		}
    },
    units: {
        type: [Object], optional: true, defaultValue: []
    },
    'units.$._id': {
        type: String,
        optional: true
    },
    'units.$.unitNumber': {
        type: String, optional: false
    },
    'units.$.squareFootage': {
        type: Number, optional: false,
    },
    'units.$.residents': {
        type: Number, optional: true, defaultValue: 0
    },
    'units.$.tenant': {
        type: String, regEx: SimpleSchema.RegEx.Email, optional: true
    },
    'units.$.createdAt': {
        type: Date,
        optional: false
    },
    'units.$.UpdatedAt': {
        type: Date,
        optional: true
    },
    createdAt: {
        type: Date,
        optional: false,
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            }
        }
    },
    UpdatedAt: {
        type: Date,
        optional: true,
        autoValue: function() {
            if (this.isUpdate) {
                return new Date();
            }
        }
    }
});

Buildings.attachSchema( BuildingSchema );