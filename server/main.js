Meteor.startup(() => {
  // code to run on server at startup
  import { Meteor } from 'meteor/meteor';
  import { Accounts } from 'meteor/accounts-base'
  import _ from 'lodash';

  // Initialization of MAIL_URL
  process.env.MAIL_URL = "smtp://" + Meteor.settings.private.gmail.email + ":" + Meteor.settings.private.gmail.password + "@smtp.gmail.com:587";
});