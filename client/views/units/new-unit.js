import { Accounts } from 'meteor/accounts-base'

Template.NewUnit.onCreated(function(){
	var self = this;
	self.autorun(function() {
        // Template Level Subscription
		self.subscribe('FindUserByEmailId');
	});
});

Template.NewUnit.events({
	'click .fa-close': function() {
		Session.set('newUnit', false);
	},
	
    'submit form' (event, template) {
        event.preventDefault();
		var target = event.target;
        // Get the Unit's Data' from the form
        var unitNumber = target.unitNumber.value;
        var sqFootage = target.sqFootage.value;
        var residents = target.residents.value;
        var tenant = target.tenant.value;
        // Validation
        if(unitNumber.trim().length > 0 && sqFootage.trim().length >0) {
            var unitData = {
                _id: Meteor.uuid(),
                unitNumber: unitNumber,
                squareFootage: sqFootage,
                residents: residents || 0,
                createdAt: new Date()
            }
            if(tenant.trim().length > 0) {
                if(GlobalHelpers.validateEmail(tenant)) {
                    unitData.tenant = tenant;
                } else {
                    alert("Invalid Email");
                    return;
                }
            }
            // Attempt to Insert.
            Meteor.call('insertUnit', FlowRouter.getParam('id'), unitData, function(err){
                console.log("tenant: ", unitData.tenant);
                if(err) {
                    console.log("Error Occured: ", err);
                    alert("Some Fields are Missing Or Have Unmatched Types. Please Recheck and then Submit");
                    return;
                }
                if(unitData.tenant) {
                    const user = Meteor.users.findOne({
                                    emails: { 
                                        $elemMatch: { address: unitData.tenant} 
                                    }
                                });
                    console.log("user: ", user);
                    if(user) {
                        Meteor.subscribe('UpdateTenantUser', user, {
                            "tenant": true,
                            "updatedAt": new Date()
                        }, {
                            onReady: function () {
                                alert("User is already Registered and has Become a Tenant");
                                alert("Unit has been created Successfully");
                                // Clear Input Fields Finally
                                template.find("form").reset();
                                Session.set('newUnit', false);
                            },
                            onError: function () {
                                alert("Something went Wrong while making User a Tenant"); 
                            }
                        });
                        
                    } else {
                        // Generate Password for Tenant New Account 
                        var password = Random.id(8);
                        // Create Tenant Account
                        Meteor.subscribe('InsertTenantUser', unitData.tenant, password, {
                            onReady: function () {
                                // Send Password to Tenant via Email
                                Meteor.call('sendEmailToTenant', unitData.tenant, password, function(err){
                                    if(err) {
                                        console.log("Error Occured While Sending an Email to Tenant Address: ", err);
                                        alert("Error Occured While Sending an Email to Tenant Address");
                                    }
                                    console.log("An email has been sent to the Tenant Address");
                                    alert("Tenant Account has been Created And Password has been sent to the Tenant Email Address");
                                    alert("Unit has been created Successfully");
                                    // Clear Input Fields Finally
                                    template.find("form").reset();
                                    Session.set('newUnit', false);
                                });
                            },
                            onError: function () {
                                alert("Something went Wrong while Creating a Tenant User"); 
                            }
                        });
                    }
                } else {
                    alert("Unit has been created Successfully");
                    // Clear Input Fields Finally
                    template.find("form").reset();
                    Session.set('newUnit', false);
                }
            });
        } else {
            alert("Unit Number and Square Footage are Required Fields. Please Fill them");
        }
    }
});