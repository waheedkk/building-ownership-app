Template.NewPortfolio.events({
	'click .fa-close': function() {
		Session.set('newPortfolio', false);
	},
	
    'submit form' (event, template) {
        event.preventDefault();
		var target = event.target;
        // Get the Building's Name and Address from the form
		console.log("target: ", target);
        var portfolioName = target.PortfolioName.value;

        if( portfolioName.trim().length > 0 ) {
            // Attempt to Insert.
            Meteor.call('insertPortfolio', portfolioName, function(err, result){
                if(err) {
                    console.log("Error Occured: ", err);
                    alert("Portfolio must have a Name");
                    return;
                }
                Session.set('newPortfolio', false);
            });
        } else {
            alert("Portfolio must have a Name");
        }
		// Clear Input Fields Finally
		template.find("form").reset();
    }
});