Template.Header.helpers({
    loggedInUserEmail: ()=> {
        if (Meteor.user() && Meteor.user().emails){
            return Meteor.user().emails[0].address;
        }
    }
});

Template.Header.events({

    'click .logout-button-position': (event,template)=> {
        event.preventDefault();
			
         Meteor.logout(function(error) {
            if(error) {
               console.log("ERROR: " + error.reason);
               return;
            }
            FlowRouter.go("/");
         });
    }
});