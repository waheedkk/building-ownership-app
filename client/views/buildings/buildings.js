Template.Buildings.onCreated(function(){
	var self = this;
	self.autorun(function() {
		// Subscribe to Buildings
        // Template Level Subscription
		self.subscribe('buildings');
		var subs = self.subscribe('FindUserByEmailId');
	});
});

Template.Buildings.helpers({
	
	buildings: ()=> {
		return Buildings.find({
			portfolioId: FlowRouter.getParam('portfolioId')
		});
	}
});

Template.Buildings.events({
	'click .new-element': () => {
		Session.set('newBuilding', true);
	}
});