// Store Additional Fields to User Schema and After that Send a Code on Phone Number
Accounts.onCreateUser(function(options, user) {
  if(options && options.firstName && options.phoneNumber) {
    _.extend(user, {
          firstName: options.firstName,
          phoneNumber: options.phoneNumber,
          verified: false,
          owner: true,
          tenant: false,
          createdAt: new Date(),
          UpdatedAt: new Date()
    });
  } else {
    _.extend(user, {
          verified: false,
          owner: false,
          tenant: true,
          createdAt: new Date(),
          UpdatedAt: new Date()
    });
  }

    return user;
});
