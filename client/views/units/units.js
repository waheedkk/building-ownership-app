// Subscribe to Buildings
Template.Units.onCreated(function(){
	var self = this;
	self.autorun(function() {
        var buildingId = FlowRouter.getParam('id');
		self.subscribe('singleBuilding', buildingId);
	});
});

Template.Units.helpers({
	units: ()=> {
		var buildingId = FlowRouter.getParam('id');
		var building = Buildings.findOne({_id: buildingId});
        if(building.units && building.units.length > 0) {
            return building.units;
        }
        return [];
	}
});

Template.Units.events({
	'click .new-element': () => {
		Session.set('newUnit', true);
	}
});