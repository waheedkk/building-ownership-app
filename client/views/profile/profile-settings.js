Template.ProfileSettings.onCreated(function(){
	var self = this;
	self.autorun(function() {
        // Template Level Subscription
		self.subscribe('FindUserByEmailId');
	});
});

Template.ProfileSettings.helpers({
    currentPhoneNumber: ()=> {
		return Meteor.user().phoneNumber;
	}
});

Template.ProfileSettings.events({
    
    'submit form' (event, template) {
        event.preventDefault();
		var target = event.target;
        // Get the Unit's Data' from the form
        var phoneNumber = target.updatedPhoneNumber.value;
        // Validation
        if(phoneNumber.trim().length > 0) {

            //First Check If the Current Number and Updated Number are Same or not
            if(phoneNumber === Meteor.user().phoneNumber) {
                alert("Same Number");
                return;
            }
            // Call Nexmo Method and get a Verification Code
            Meteor.call('getVerificationRequestId', phoneNumber, function(err, VerificationRequestId){
                if(err) {
                    console.log("err: ", err);
                    alert("Something Went Wrong while Processing Phone Number");
                    return;
                } else {
                    console.log("VerificationRequestId: ", VerificationRequestId);

                    Meteor.subscribe('UpdateCurrentUser', {
                        "phoneNumber": phoneNumber,
                        "emails.0.verified": false,
                        "verified": false,
                        "updatedAt": new Date()
                    }, {
                        onReady: function () {
                            alert("An activation Code has been sent to your Number. Please Check");
                            FlowRouter.go("/activate");
                        },
                        onError: function () {
                            alert("Something went Wrong while making User a Tenant"); 
                        }
                    });
                }
            });
            // Clear Input Fields Finally
            template.find("form").reset();
        } else {
            alert("Phone Number is Required");
        }
    }
});