Portfolios = new Mongo.Collection('portfolios');

Portfolios.allow({
    insert: function(userId, doc) {
        return !!userId;
    },
	update: function(userId, doc) {
		return !!userId;
	}
});


PortfolioSchema = new SimpleSchema({
    
    name: {
        type: String, optional: false
    },
    ownerId: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: false,
        autoValue: function () {
			return this.userId
		}
    },
    createdAt: {
        type: Date,
        optional: false,
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            }
        }
    },
    UpdatedAt: {
        type: Date,
        optional: true,
        autoValue: function() {
            if (this.isUpdate) {
                return new Date();
            }
        }
    }
});

Portfolios.attachSchema( PortfolioSchema );